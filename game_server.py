import argparse
import json
import importlib

import configuration
configuration.configure()

from configuration import settings
from game_server_core.game_server_core import GameServerCore
from game_server_core.network_strategy_adapter import NetworkStrategyAdapter

def on_game_over(log_data, result):
    with open('game_log', 'bw') as log_file:
        log_file.write(log_data)
    print("[GAME_RESULT] {}".format(result))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--strategies_ids', metavar='strategy_id', type=int,
                        nargs='+', required=True,
                        help='List of strategies ids')
    parser.add_argument('--mechanic', metavar='mechanic_name', type=str,
                        required=True, help='Mechanic')

    args = parser.parse_args()

    mechanic_name = args.mechanic

    mechanic_path = "{}.game.mechanic".format(mechanic_name)
    mechanic = importlib.import_module(mechanic_path)

    strategies_adapters = []

    for strategy_id in args.strategies_ids:
        adapter = NetworkStrategyAdapter(player_id=strategy_id, port=0)
        port = adapter.sock.getsockname()[1]
        print("[STRATEGY_PORT] {}:{}".format(strategy_id, port))
        strategies_adapters.append(adapter)

    game_server_core = GameServerCore(
        mechanic.Mechanic,
        strategies_adapters,
        on_game_over = on_game_over
    )
    game_server_core.start()

GAME_SERVER_CORE_PATH = '..'
MECHANICS_PATH = '../mechanics/'

try:
    from settings import *
except ImportError:
    print('settings.py not found. Using default settings.')
